import React from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import setToken from "./helpers/setToken";

import HeaderComponent from "./component/staticviews/HeaderComponent";
import FooterComponent from "./component/staticviews/FooterComponent";

import AdminArticlePage from "./component/views/AdminArticlePage";
import AdminStoryPage from "./component/views/AdminStoryPage";
import CommunityPage from "./component/views/CommunityPage";
import ContentPage from "./component/views/ContentPage";
import DetailContentPage from "./component/views/DetailContentPage";
import DetailStoryPage from "./component/views/DetailStoryPage";
import DashboardPage from "./component/views/DashboardPage";
import ProfilePage from "./component/views/ProfilePage";
import ProfileEditPage from "./component/views/ProfileEditPage";
import ProfilePostPage from "./component/views/ProfilePostPage";
import SigninPage from "./component/views/SigninPage";
import SignupPage from "./component/views/SignupPage";
import StoryPage from "./component/views/StoryPage";
import theme from "./helpers/theme";
import { MuiThemeProvider } from "@material-ui/core";

const AppRoute = ({ component: Component, layout: Layout, auth: Auth, ...rest }) => (
  <Auth>
    <Route
      {...rest}
      render={props => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  </Auth>
);

const MainLayout = props => (
  <div>
    <HeaderComponent />
    {props.children}
    <FooterComponent />
  </div>
);

const Guest = props => props.children;

const User = props => (sessionStorage.token ? props.children : <Redirect to="/" />);

const Admin = props =>
  sessionStorage.token && sessionStorage.isAdmin ? props.children : <Redirect to="/" />;

if (sessionStorage.token) {
  setToken(sessionStorage.token);
}

const App = () => (
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <Router>
        <Switch>
          <AppRoute exact path="/" layout={MainLayout} component={DashboardPage} auth={Guest} />
          <AppRoute
            exact
            path="/admin/article"
            layout={MainLayout}
            component={AdminArticlePage}
            auth={Admin}
          />
          <AppRoute
            exact
            path="/admin/story"
            layout={MainLayout}
            component={AdminStoryPage}
            auth={Admin}
          />
          <AppRoute
            exact
            path="/content"
            layout={MainLayout}
            component={ContentPage}
            auth={Guest}
          />
          <AppRoute
            exact
            path="/content/:id"
            layout={MainLayout}
            component={DetailContentPage}
            auth={Guest}
          />
          <AppRoute
            exact
            path="/community"
            layout={MainLayout}
            component={CommunityPage}
            auth={Guest}
          />
          <AppRoute exact path="/login" layout={MainLayout} component={SigninPage} auth={Guest} />
          <AppRoute exact path="/story" layout={MainLayout} component={StoryPage} auth={Guest} />
          <AppRoute
            exact
            path="/story/:id"
            layout={MainLayout}
            component={DetailStoryPage}
            auth={Guest}
          />
          <AppRoute exact path="/profile" layout={MainLayout} component={ProfilePage} auth={User} />
          <AppRoute
            exact
            path="/myprofile/edit"
            layout={MainLayout}
            component={ProfileEditPage}
            auth={User}
          />
          <AppRoute
            exact
            path="/myprofile/post"
            layout={MainLayout}
            component={ProfilePostPage}
            auth={User}
          />
          <AppRoute
            exact
            path="/register"
            layout={MainLayout}
            component={SignupPage}
            auth={Guest}
          />
        </Switch>
      </Router>
    </Provider>
  </MuiThemeProvider>
);

export default App;

// export default class App extends Component {
//   render() {
//     return (
//       <MuiThemeProvider theme={theme}>
//         <Provider store={store}>
//           <Router>
//             <Route path="/" exact component={DashboardPage} />
//             <Route path="/content" exact component={ContentPage} />
//             <Route path="/content/:id" exact component={DetailContentPage} />
//             <Route path="/community" exact component={CommunityPage} />
//             <Route path="/login" exact component={SigninPage} />
//             <Route path="/story" exact component={StoryPage} />
//             <Route path="/profile" exact component={ProfilePage} />
//             <Route path="/profile/:id" exact component={ProfileByIdPage} />
//             <Route path="/register" exact component={SignupPage} />
//           </Router>
//         </Provider>
//       </MuiThemeProvider>
//     );
//   }
// }
