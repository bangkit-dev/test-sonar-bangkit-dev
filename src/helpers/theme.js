import { createMuiTheme } from "@material-ui/core";

export default createMuiTheme({
  palette: {
    primary: {
      light: "#00695f",
      main: "#4db6ac",
      dark: "#33ab9f",
      contrastText: "#fff"
    },
    secondary: {
      light: "#1c54b2",
      main: "#2979ff",
      dark: "#5393ff",
      contrastText: "#fff"
    }
  }
});
