import axios from 'axios'
import setToken from '../../helpers/setToken'

export const getContentProfile = () => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token)
  }
  try {
    const res = await axios.get(
      'https://glints-redu.herokuapp.com/api/contents/mine'
    )
    dispatch({
      type: 'GET_CONTENT_PROFILE',
      payload: res.data
    })
  }
  catch(error) {
    console.log(error.response.data)
  }
}
