import axios from "axios";
import setToken from "../../../helpers/setToken";

export const getListCommunity = () => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get("https://glints-redu.herokuapp.com/api/communities");
    dispatch({
      type: "GET_LIST_COMMUNITY",
      payload: res.data
    });
  } catch (error) {
    console.log(error.response.data);
  }
};
