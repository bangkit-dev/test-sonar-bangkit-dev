import axios from "axios";
import setToken from "../../helpers/setToken";

export const getProfile = () => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get(
      "https://glints-redu.herokuapp.com/api/users/me"
    );
    dispatch({
      type: "GET_PROFILE",
      payload: res.data
    });
  } catch (error) {
    console.log(error.response.data);
  }
};

export const getProfileById = id => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get(
      `https://glints-redu.herokuapp.com/api/users/detail/${id}`
    );
    dispatch({
      type: "GET_PROFILE_BY_ID",
      payload: res.data
    });
  } catch (error) {
    console.log(error.response.data);
  }
};

export const editMyProfile = (data, dataImage) => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const response = await axios.put(
      "https://glints-redu.herokuapp.com/api/users/update",
      data
    );
    const res = await axios.put(
      `https://glints-redu.herokuapp.com/api/users/pict`,
      dataImage.file
    );
    dispatch({
      type: "EDIT_MY_PROFILE",
      payload: res.data
    });
    console.log("Edit Profile", response.data);
  } catch (error) {
    console.log(error.response);
  }
};
