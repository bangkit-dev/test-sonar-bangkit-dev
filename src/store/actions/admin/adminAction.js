import axios from "axios";
import setToken from "../../../helpers/setToken";

export const getStoryAdmin = () => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get(
      "https://glints-redu.herokuapp.com/api/stories/getadmin"
    );
    dispatch({
      type: "GET_STORY_ADMIN",
      payload: res.data
    });
  } catch (error) {
    console.log(error.response.data);
  }
};

export const postStoryAdmin = (data, dataImage) => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const response = await axios.post(
      "https://glints-redu.herokuapp.com/api/contents",
      data
    );
    const id = response.data.data._id;
    const res = await axios.put(
      `https://glints-redu.herokuapp.com/api/contents/pict/${id}`,
      dataImage.file
    );
    dispatch({
      type: "POST_STORY_ADMIN",
      payload: res.data
    });
    console.log("Post Story Admin", response.data);
  } catch (error) {
    console.log(error.response);
  }
};

export const publishStoryAdmin = (data, id) => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.put(
      `https://glints-redu.herokuapp.com/api/stories/publish/${id}`,
      data
    );
    dispatch({
      type: "PUBLISH_STORY_ADMIN",
      payload: res.data
    });
    console.log("Publish Story Admin", res.data);
  } catch (error) {
    console.log(error.response);
  }
};

export const deleteStoryAdmin = id => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.delete(
      `https://glints-redu.herokuapp.com/api/stories/delete/${id}`
    );
    dispatch({
      type: "DELETE",
      payload: res.data
    });
    console.log("Delete Story Admin", res.data);
  } catch (error) {
    console.log(error.response);
  }
};
