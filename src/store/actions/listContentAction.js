import axios from "axios";
import setToken from "../../helpers/setToken";

export const getListContent = () => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get("https://glints-redu.herokuapp.com/api/contents");
    dispatch({
      type: "GET_LIST_CONTENT",
      payload: res.data
    });
  } catch (error) {
    console.log(error);
  }
};

export const postContent = postContent => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }

  try {
    const res = await axios.post("https://glints-redu.herokuapp.com/api/contents", postContent);
    dispatch({
      type: "POST_CONTENT",
      payload: res.data
    });
  } catch (error) {
    console.log(error);
  }
};

export const getContentById = id => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get(`https://glints-redu.herokuapp.com/api/contents/detail/${id}`);
    dispatch({
      type: "GET_CONTENT_BY_ID",
      payload: res.data
    });
  } catch (error) {
    console.log(error);
  }
};

export const postComment = (postComment, id) => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }

  try {
    const res = await axios.post(
      `https://glints-redu.herokuapp.com/api/comments?content=${id}`,
      postComment
    );
    dispatch({
      type: "POST_COMMENT",
      payload: res.data
    });
  } catch (error) {
    console.log(error);
  }
};
