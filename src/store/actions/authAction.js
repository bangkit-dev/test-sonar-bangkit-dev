import axios from "axios";

export const register = formData => async dispatch => {
  try {
    const res = await axios.post(
      "https://glints-redu.herokuapp.com/api/users",
      formData
    );
    dispatch({
      type: "REGISTER_SUCCESS",
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: "REGISTER_FAIL",
      payload: error.response.data
    });
  }
};

export const login = formData => async dispatch => {
  try {
    const res = await axios.post(
      "https://glints-redu.herokuapp.com/api/users/login",
      formData
    );
    dispatch({
      type: "REGISTER_SUCCESS",
      payload: res.data
    });
    console.log(res);
  } catch (error) {
    dispatch({
      type: "REGISTER_FAIL",
      payload: error.response.data
    });
  }
};
