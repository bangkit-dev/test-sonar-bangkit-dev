import axios from "axios";
import setToken from "../../../helpers/setToken";

export const getListStory = () => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get("https://glints-redu.herokuapp.com/api/stories");
    dispatch({
      type: "GET_LIST_STORY",
      payload: res.data
    });
  } catch (error) {
    console.log(error.response.data);
  }
};

export const getMyStory = () => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get("https://glints-redu.herokuapp.com/api/stories/mine");

    dispatch({
      type: "GET_MY_STORY",
      payload: res.data
    });
  } catch (error) {
    console.log(error.response.data);
  }
};

export const postMyStory = (data, dataImage) => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const response = await axios.post("https://glints-redu.herokuapp.com/api/stories", data);
    const id = response.data.data._id;
    const res = await axios.put(
      `https://glints-redu.herokuapp.com/api/stories/pict/${id}`,
      dataImage.file
    );
    dispatch({
      type: "POST_MY_STORY",
      payload: res.data
    });
  } catch (error) {
    console.log(error.response);
  }
};

export const getStoryById = id => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const res = await axios.get(`https://glints-redu.herokuapp.com/api/stories/detail/${id}`);
    dispatch({
      type: "GET_STORY_BY_ID",
      payload: res.data
    });
  } catch (error) {
    console.log(error.response);
    console.log("Error");
  }
};
