import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
//import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers";

const middleware = [thunk];

// Development
//const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)));

// Production
const store = createStore(rootReducer, applyMiddleware(...middleware));

export default store;
