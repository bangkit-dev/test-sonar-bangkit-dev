const initialState = {
  detailContent: {},
  listComment: []
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "GET_CONTENT_BY_ID":
      return {
        ...state,
        ...payload,
        detailContent: payload.content,
        listComment: payload.comment
      };
    case "POST_COMMENT":
      return {
        ...state,
        listComment: [...state.listComment, payload.data],
        response: true
      };
    default:
      return state;
  }
}
