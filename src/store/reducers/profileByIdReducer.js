const initialState = {
  profile: {}
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case 'GET_PROFILE_BY_ID':
      return {
        ...state,
        ...payload,
        profile: payload.data
      }
    default:
      return state
  }
}
