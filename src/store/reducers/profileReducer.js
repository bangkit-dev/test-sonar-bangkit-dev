const initialState = {
  data: {},
  editedProfile: {}
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "GET_PROFILE":
      return {
        ...state,
        ...payload,
        data: payload.data
      };
    case "EDIT_MY_PROFILE":
      return {
        ...state,
        ...payload,
        editedProfile: payload.data
      };
    default:
      return state;
  }
}
