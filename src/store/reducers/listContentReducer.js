const initialState = {
  listContent: [],
  loading: true,
  response: null
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "GET_LIST_CONTENT":
      return {
        ...state,
        ...payload,
        listContent: payload.data,
        loading: false
      };
    case "POST_CONTENT":
      return {
        ...state,
        listContent: [...state.listContent, payload.data],
        response: true
      };
    default:
      return state;
  }
}
