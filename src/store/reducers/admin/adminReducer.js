const initialState = {
  listStoryAdmin: [],
  response: null
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "GET_STORY_ADMIN":
      return {
        ...state,
        ...payload,
        listStoryAdmin: payload
      };
    default:
      return state;
  }
}
