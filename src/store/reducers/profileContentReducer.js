const initialState = {
  contentProfile: []
}

export default function (state = initialState, action) {
  const { type, payload } = action

  switch (type) {
    case 'GET_CONTENT_PROFILE':
      return {
        ...state,
        ...payload,
        contentProfile: payload.data
      }
    default:
      return state
  }
}
