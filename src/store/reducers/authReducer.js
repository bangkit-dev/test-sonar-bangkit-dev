const initialState = {
  token: sessionStorage.getItem("token"),
  isAdmin: null,
  isAuthenticated: null,
  loading: true,
  error: []
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "REGISTER_SUCCESS":
      sessionStorage.setItem("token", payload.data);
      sessionStorage.setItem("isAdmin", payload.isAdmin);
      return {
        ...state,
        ...payload,
        isAuthenticated: true,
        loading: false
      };
    case "REGISTER_FAIL":
      sessionStorage.removeItem("token", payload.data);
      return {
        ...state,
        isAuthenticated: false,
        loading: false,
        error: payload.message
      };
    default:
      return state;
  }
}
