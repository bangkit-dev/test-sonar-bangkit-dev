const initialState = {
  listStory: [],
  detailStory: {},
  storyComment: {},
  myStory: {},
  postMyStory: {},
  loading: true,
  response: null
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "GET_LIST_STORY":
      return {
        ...state,
        ...payload,
        listStory: payload.data,
        loading: false
      };
    case "GET_MY_STORY":
      return {
        ...state,
        ...payload,
        myStory: payload.data
      };
    case "GET_STORY_BY_ID":
      return {
        ...state,
        ...payload,
        detailStory: payload.story,
        storyComment: payload.comment
      };
    case "POST_MY_STORY":
      return {
        ...state,
        ...payload,
        postMyStory: payload.data
      };
    default:
      return state;
  }
}
