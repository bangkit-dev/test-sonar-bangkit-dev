import { combineReducers } from "redux";
import authReducer from "./authReducer";
import adminReducer from "./admin/adminReducer";
import communityReducer from "./community/communityReducer";
import contentByCategoryReducer from "./contentByCategoryReducer";
import detailContentReducer from "./detailContentReducer";
import listContentReducer from "./listContentReducer";
import listStoryReducer from "./story/listStoryReducer";
import profileReducer from "./profileReducer";
import profileByIdReducer from "./profileByIdReducer";
import profileContentReducer from "./profileContentReducer";

export default combineReducers({
  authReducer,
  adminReducer,
  communityReducer,
  contentByCategoryReducer,
  detailContentReducer,
  listContentReducer,
  listStoryReducer,
  profileReducer,
  profileByIdReducer,
  profileContentReducer
});
