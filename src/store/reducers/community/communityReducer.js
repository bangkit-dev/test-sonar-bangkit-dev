const initialState = {
  listCommunity: [],
  response: null
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "GET_LIST_COMMUNITY":
      return {
        ...state,
        ...payload,
        listCommunity: payload.data
      };
    default:
      return state;
  }
}
