import React, { Component } from "react";
import "../assets/scss/CardComponent.scss";
import { Link } from "react-router-dom";

class CardComponent extends Component {
  render() {
    return (
      <div className="card--section">
        <div className="card--container">
          <div className="card--title">
            <h3 style={{ color: "#2e2e2e" }}>Explore more with redu</h3>
          </div>
          <div className="card--items">
            <Link className="decor-none" to="/content">
              <div className="card--item left">
                <div className="card-item-img">
                  <img
                    src={require("../assets/images/undraw_adventure_4hum.svg")}
                    alt="Superior REDU"
                  />
                </div>
                <div className="card-item-title">
                  <h1>Article</h1>
                  <p>
                    We know many great place for you to explore. From the popular ones, until the
                    hidden ones. We give you full insight from the food, the culture, even the
                    handcraft. We create the article based on our mission, to share the recreation
                    that give education and knowledge for the visitor.
                  </p>
                </div>
              </div>
            </Link>
            <Link className="decor-none" to="/story">
              <div className="card--item middle">
                <div className="card-item-img">
                  <img
                    src={require("../assets/images/undraw_traveling_t8y2.svg")}
                    alt="Superior REDU"
                  />
                </div>
                <div className="card-item-title">
                  <h1>Story</h1>
                  <p>
                    Travel story isn't just about your holiday. Story give detail about how people
                    travel, when the right time to travel, even the budget ones. We want other
                    people to know about your story, and enhance them to go to the destinations. We
                    want to promote every destinations, and give readers good story to experience
                    the destination before they arrive.
                  </p>
                </div>
              </div>
            </Link>
            <Link className="decor-none" to="/community">
              <div className="card--item right">
                <div className="card-item-img">
                  <img
                    src={require("../assets/images/undraw_Waiting__for_you_ldha.svg")}
                    alt="Superior REDU"
                  />
                </div>
                <div className="card-item-title">
                  <h1>Community</h1>
                  <p>
                    There are many community in every destinations. And it will be good for you to
                    ask every details about your destination from the community. That's why we
                    collect every community information and share it with you. Don't hestitate to
                    contact them, and start your journey.
                  </p>
                </div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default CardComponent;
