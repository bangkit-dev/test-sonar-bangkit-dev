import React, { Component } from "react";
import AdminStoryComponent from "../admin/AdminStoryComponent";

class AdminStoryPage extends Component {
  render() {
    return (
      <div>
        <AdminStoryComponent />
      </div>
    );
  }
}

export default AdminStoryPage;
