import React, { Component } from "react";
import AdminArticleComponent from "../admin/AdminArticleComponent";

class AdminArticlePage extends Component {
  render() {
    return (
      <div>
        <AdminArticleComponent />
      </div>
    );
  }
}

export default AdminArticlePage;
