import React, { Component } from "react";
import CommunityComponent from "../community/CommunityComponent";

class CommunityPage extends Component {
  render() {
    return (
      <div>
        <CommunityComponent />
      </div>
    );
  }
}

export default CommunityPage;
