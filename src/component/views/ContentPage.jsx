import React, { Component } from "react";
import ArticleComponent from "../article/ArticleComponent";

class ContentPage extends Component {
  render() {
    return (
      <div>
        <ArticleComponent />
      </div>
    );
  }
}

export default ContentPage;
