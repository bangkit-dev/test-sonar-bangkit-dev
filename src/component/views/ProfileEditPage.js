import React, { Component } from "react";
import ProfileEditComponent from "../profile/ProfileEditComponent";

class ProfileEditPage extends Component {
  render() {
    return (
      <div>
        <ProfileEditComponent />
      </div>
    );
  }
}

export default ProfileEditPage;
