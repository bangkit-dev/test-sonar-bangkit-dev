import React, { Component } from "react";
import SignupComponent from "../SignupComponent";

class SignupPage extends Component {
  render() {
    return (
      <div>
        <SignupComponent />
      </div>
    );
  }
}

export default SignupPage;
