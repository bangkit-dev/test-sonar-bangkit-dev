import React, { Component } from "react";
import DetailStoryComponent from "../story/DetailStoryComponent";

class DetailStoryPage extends Component {
  render() {
    return (
      <div>
        <DetailStoryComponent />
      </div>
    );
  }
}

export default DetailStoryPage;
