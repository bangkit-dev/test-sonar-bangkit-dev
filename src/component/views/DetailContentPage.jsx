import React, { Component } from "react";
import DetailArticleComponent from "../article/DetailArticleComponent";

class DetailContentPage extends Component {
  render() {
    return (
      <div>
        <DetailArticleComponent />
      </div>
    );
  }
}

export default DetailContentPage;
