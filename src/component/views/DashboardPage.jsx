import React, { Component } from "react";
import LandingComponent from "../LandingComponent";
import CardComponent from "../CardComponent";
import TutorialComponent from "../TutorialComponent";

class DashboardPage extends Component {
  render() {
    return (
      <div>
        <LandingComponent />
        <CardComponent />
        <TutorialComponent />
      </div>
    );
  }
}

export default DashboardPage;
