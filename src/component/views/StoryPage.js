import React, { Component } from "react";
import StoryComponent from "../story/StoryComponent";

class StoryPage extends Component {
  render() {
    return (
      <div>
        <StoryComponent />
      </div>
    );
  }
}

export default StoryPage;
