import React, { Component } from "react";
import ProfilePostComponent from "../profile/ProfilePostComponent";

class ProfilePostPage extends Component {
  render() {
    return (
      <div>
        <ProfilePostComponent />
      </div>
    );
  }
}

export default ProfilePostPage;
