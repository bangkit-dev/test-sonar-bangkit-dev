import React, { Component } from 'react'
import NotFoundComponent from '../NotFoundComponent';

class NotFound extends Component {
  render() {
    return (
      <div>
        <NotFoundComponent />
      </div>
    )
  }
}

export default NotFound
