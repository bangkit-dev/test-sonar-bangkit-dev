import React, { Component } from "react";
import SigninComponent from "../SigninComponent";

class SigninPage extends Component {
  render() {
    return (
      <div>
        <SigninComponent />
      </div>
    );
  }
}

export default SigninPage;
