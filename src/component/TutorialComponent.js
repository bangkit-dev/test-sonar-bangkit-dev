import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../assets/scss/TutorialComponent.scss";

class TutorialComponent extends Component {
  render() {
    return (
      <div className="tutorial--section">
        <div className="tutorial--container">
          <div className="tutorial--title">
            <h1>Get started</h1>
            <p> Gentle introduction to REDU.</p>
          </div>
          <div className="tutorial--contents">
            <div className="tutorial-content-img">
              <img
                src={require("../assets/images/designecologist-p2DItjy2IWQ-unsplash.jpg")}
                alt="Superior REDU"
              />
            </div>
            <div className="tutorial-content-desc">
              <div className="tutorial--list">
                <img src={require("../assets/images/number_1.png")} alt="Number 1" />
                <p>Access REDU from you browser or download the app .</p>
              </div>
              <div className="tutorial--list">
                <img src={require("../assets/images/number_2.png")} alt="Number 2" />
                <p>Find and read interesting travel story from around the world </p>
              </div>
              <div className="tutorial--list">
                <img src={require("../assets/images/number_3.png")} alt="Number 3" />
                <p>Register and share your own story.</p>
              </div>
              <div className="tutorial--list">
                <img src={require("../assets/images/number_4.png")} alt="Number 4" />
                <p>Join the community and explore more.</p>
              </div>
              <Link to="/register" className="tutorial--register">
                <button className="btn-register">Join now</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TutorialComponent;
