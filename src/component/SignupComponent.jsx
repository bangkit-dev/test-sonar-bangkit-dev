import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { register } from "../store/actions/authAction";
import "../assets/scss/SignupComponent.scss";

class SignupComponent extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      interest: [],
      gender: "",
      setFavorite: false,
      setGender: false
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = async e => {
    e.preventDefault();

    const formData = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      interest: this.state.interest,
      gender: this.state.gender
    };

    await this.props.register(formData);
    if (this.props.isAuthenticated) {
      this.props.history.push("/login");
    } else {
      this.setState({
        error: true
      });
    }
  };

  changeGender = () => {
    this.setState({
      setGender: true
    });
  };

  removeGender = () => {
    if (this.state.setGender === false) {
      return (
        <select name="gender" id="" onChange={this.handleChange}>
          <option value="0">Gender:</option>
          <option value="male">Male</option>
          <option value="female">Female</option>
        </select>
      );
    } else {
      return (
        <select name="gender" id="" onChange={this.handleChange}>
          <option value="male">Male</option>
          <option value="female">Female</option>
        </select>
      );
    }
  };

  changeInterest = () => {
    this.setState({
      setFavorite: true
    });
  };

  changeValue = () => {
    if (this.state.setFavorite === false) {
      return (
        <select name="interest" id="" onChange={this.handleChange}>
          <option value="0">Interest:</option>
          <option value="City">City</option>
          <option value="Culinary">Culinary</option>
          <option value="Culture">Culture</option>
          <option value="Historical">Historical</option>
          <option value="Religious">Religious</option>
          <option value="Tourism">Tourism</option>
        </select>
      );
    } else {
      return (
        <select name="interest" id="" onChange={this.handleChange}>
          <option value="City">City</option>
          <option value="Culinary">Culinary</option>
          <option value="Culture">Culture</option>
          <option value="Historical">Historical</option>
          <option value="Religious">Religious</option>
          <option value="Tourism">Tourism</option>
        </select>
      );
    }
  };

  render() {
    const errorList = () => {
      if (this.state.error === true) return <p>{this.props.error}</p>;
    };
    return (
      <div className="signup-container">
        <div className="signup-page">
          <div className="form">
            <h1>Register</h1>
            {errorList()}
            <form className="signup-form" onSubmit={this.handleSubmit}>
              <input
                type="text"
                placeholder="Name"
                name="name"
                value={this.state.name}
                onChange={this.handleChange}
              />
              <input
                type="email"
                placeholder="Email"
                name="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
              <input
                type="password"
                placeholder="Password"
                name="password"
                value={this.state.password}
                onChange={this.handleChange}
              />
              <div className="flex--split">
                <div className="select--gender" onClick={this.changeGender}>
                  {this.removeGender()}
                </div>
                <div className="select--interest" onClick={this.changeInterest}>
                  {this.changeValue()}
                </div>
              </div>
              <button type="submit">Register</button>

              <p className="message">
                Already member? <a href="/login">Sign in</a>
              </p>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    error: state.authReducer.error
  };
};

export default connect(
  mapStateToProps,
  { register }
)(withRouter(SignupComponent));
