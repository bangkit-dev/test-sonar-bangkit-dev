import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import { login } from "../store/actions/authAction";
import "../assets/scss/SigninComponent.scss";

class SigninComponent extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      loading: false
    };
  }

  loading = isLoading => {
    if (isLoading) {
      return (
        <img
          src={require("../assets/images/Spinner-1s-200px.gif")}
          style={{ textAlign: "center", width: "30px" }}
          alt="Loading"
        />
      );
    }
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = async e => {
    e.preventDefault();

    this.setState({
      loading: true
    });

    const formData = {
      email: this.state.email,
      password: this.state.password,
      error: null
    };

    await this.props.login(formData);
    if (this.props.isAuthenticated && this.props.isAdmin) {
      this.props.history.push("/admin/article");
    } else if (this.props.isAuthenticated) {
      this.props.history.push("/");
    } else {
      this.setState({
        error: true
      });
      this.setState({
        email: "",
        password: ""
      });
    }
  };

  render() {
    const errorList = () => {
      if (this.state.error === true) return <p>{this.props.error}</p>;
    };
    return (
      <div className="login-container">
        <div className="login-page">
          <div className="form">
            <h1>Login</h1>
            {errorList()}
            <form className="login-form" onSubmit={this.handleSubmit}>
              <input
                type="email"
                name="email"
                placeholder="Email"
                value={this.state.email}
                onChange={this.handleChange}
              />
              <input
                type="password"
                name="password"
                placeholder="Password"
                value={this.state.password}
                onChange={this.handleChange}
              />
              <div className="check">
                <input type="checkbox" /> Remember me
                <br />
              </div>
              <div className="check">
                <Link to="#">Forgot Password</Link>
              </div>

              <button type="submit">
                {this.loading(this.state.loading)}
                <span className={this.state.loading ? "vhidden" : "white"}>Log in</span>
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    loading: state.authReducer.loading,
    isAdmin: state.authReducer.isAdmin,
    error: state.authReducer.error
  };
};

export default connect(
  mapStateToProps,
  { login }
)(withRouter(SigninComponent));
