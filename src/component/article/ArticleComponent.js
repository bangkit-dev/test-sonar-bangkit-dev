import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getListContent } from "../../store/actions/listContentAction";
import "../../assets/scss/ArticleComponent.scss";

class ArticleComponent extends Component {
  componentDidMount() {
    this.props.getListContent();
  }

  loading = () => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
          margin: "auto"
        }}
      >
        <img
          src={require("../../assets/images/Spinner-1s-200px.gif")}
          style={{ textAlign: "center" }}
          alt="Loading"
        />
      </div>
    );
  };

  render() {
    const listContent = this.props.listContent.map(lists => {
      return (
        <Link className="default-link" key={lists._id} to={`/content/${lists._id}`}>
          <div className="article-lists-item">
            <img src={lists.photo && lists.photo.secure_url} alt={lists.title} />
            <h3>{lists.title}</h3>
          </div>
        </Link>
      );
    });

    return (
      <div className="article-section-wrap">
        <div className="article--category">
          <div className="article--container">
            <div className="article-category-list">
              <p>All</p>
              <p>City</p>
              <p>Culinary</p>
              <p>Culture</p>
              <p>Historical</p>
              <p>Natural</p>
              <p>Religious</p>
              <p>Tourism</p>
            </div>
          </div>
        </div>
        <div className="article--lists">
          <div className="article-lists-title">
            <h1>You aren’t being anyone yet. Here are some popular article instead.</h1>
            <p>Find friends or object recreation from this site.</p>
          </div>
          <div className="article-lists-items">
            {listContent.length >= 1 ? (
              listContent
            ) : this.props.loading ? (
              this.loading()
            ) : (
              <p className="Grey">Article is empty...</p>
            )}
          </div>
          <br />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    listContent: state.listContentReducer.listContent,
    loading: state.listContentReducer.loading
  };
};

export default connect(
  mapStateToProps,
  { getListContent }
)(ArticleComponent);
