import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getContentById } from "../../store/actions/listContentAction";
import { getProfile } from "../../store/actions/profileAction";
import { postComment } from "../../store/actions/listContentAction";
import "../../assets/scss/DetailArticleComponent.scss";

class DetailArticleComponent extends Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.getContentById(id);
    this.props.getProfile();
  }

  commentProfile = () => {
    if (this.props.isAuthenticated || sessionStorage.token != null) {
      return (
        <img
          src={this.props.profile.photo && this.props.profile.photo.secure_url}
          alt={this.props.profile.name}
        />
      );
    } else {
      return (
        <img
          src={require("../../assets/images/user-png-icon-male-user-icon-512.png")}
          alt="Login First"
        />
      );
    }
  };

  constructor() {
    super();
    this.state = {
      text: ""
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = async (e, getID) => {
    e.preventDefault();
    const postComment = {
      text: this.state.text
    };

    this.props.postComment(postComment, this.props.data._id);
    this.setState({
      text: ""
    });
  };

  render() {
    console.log(this.state.text);
    const { title, photo, body, category, _user, location, _id } = this.props.data;
    const listComment = this.props.comment.map(lists => {
      return (
        <div className="detail-comment-user" key={lists._id}>
          <div className="profile" key={lists._user && lists._user._id}>
            {lists._user.photo ? (
              <img
                src={lists._user ? lists._user.photo.secure_url : ""}
                alt={lists._user && lists._user.name}
              />
            ) : null}
          </div>
          <div className="comment">
            <div className="splitter">
              <h4>{lists._user && lists._user.name}</h4>
              <p>{lists.text}</p>
            </div>
            <div className="delete">
              <span className="ti-delete"></span>
            </div>
          </div>
        </div>
      );
    });
    return (
      <div className="detail-article-section">
        <div className="detail--container">
          <div className="detail--items">
            <div className="detail-item-img">
              <img src={photo && photo.secure_url} alt={title} />
            </div>
            <div className="detail-item-article">
              <div className="profile">
                <img src={_user && _user.photo.secure_url} alt="John Doe" />
                <h4>{_user && _user.name}</h4>
              </div>
              <div className="article">
                <h3>{title}</h3>
                <p className="category">
                  Category: <span>{category}.</span> On {location}
                </p>
                <div className="box">
                  <p classname="text">{body}</p>
                </div>
              </div>
            </div>
            <div className="detail-item-comment">
              <div className="detail-comment-current">
                <div className="profile">{this.commentProfile()}</div>
                <div className="comment">
                  <textarea
                    name="text"
                    id=""
                    cols="55"
                    rows="1"
                    value={this.state.text}
                    onChange={this.handleChange}
                    placeholder="Insert what your opinion here."
                  ></textarea>
                  <button onClick={this.handleSubmit}>Comment</button>
                </div>
              </div>
              {listComment}
            </div>
          </div>
          <div className="detail-another-lists">
            <div className="detail-list-items">
              <h3>Another posts that seems like this</h3>
              <div className="detail-list-all">
                <div className="detail-list-img">
                  <img
                    src={require("../../assets/images/Tempat-Wisata Edukasi keren populer-di-Jogja museum Affandi untuk anak.jpg")}
                    alt="Superior REDU"
                  />
                </div>
                <div className="detail-list-item">
                  <h4>Museum Affandi.</h4>
                  <p className="name">Admin</p>
                  <p className="date">Posted on 07 Nov 2019</p>
                </div>
              </div>
              <div className="detail-list-all">
                <div className="detail-list-img">
                  <img
                    src={require("../../assets/images/Taman Tino Sidin di Jogja Manis dna Indah.jpg")}
                    alt="Superior REDU"
                  />
                </div>
                <div className="detail-list-item">
                  <h4>Taman Tino Sidin</h4>
                  <p className="name">Admin</p>
                  <p className="date">Posted on 07 Nov 2019</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.detailContentReducer.detailContent,
    comment: state.detailContentReducer.listComment,
    profile: state.profileReducer.data,
    isAuthenticated: state.authReducer.isAuthenticated
  };
};

export default connect(
  mapStateToProps,
  { getContentById, getProfile, postComment }
)(withRouter(DetailArticleComponent));
