import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getStoryAdmin } from "../../store/actions/admin/adminAction";
import { publishStoryAdmin } from "../../store/actions/admin/adminAction";
import { deleteStoryAdmin } from "../../store/actions/admin/adminAction";
import "../../assets/scss/AdminStoryComponent.scss";

class AdminStoryComponent extends Component {
  componentDidMount() {
    this.props.getStoryAdmin();
    this.props.publishStoryAdmin();
  }

  constructor() {
    super();
    this.state = {
      isPublished: false,
      id: ""
    };
  }

  handlePublish = async (e, getID) => {
    e.preventDefault();

    console.log(e);

    const data = {
      isPublished: this.state.isPublished
    };
    const id = getID;
    await this.props.publishStoryAdmin(data, id);
    alert("Published");
    window.location.reload("/admin/story");
  };

  handleDelete = async (e, getID) => {
    e.preventDefault();
    const id = getID;
    await this.props.deleteStoryAdmin(id);
    alert("Rejected");
    window.location.reload("/admin/story");
  };

  render() {
    console.log(this.props.data);
    const listPublish = this.props.data.map(lists => {
      return (
        <tbody key={lists._id}>
          <td>Coder Macau</td>
          <td>{lists.title}</td>
          <td>{lists.body}</td>
          <td>
            <img src={lists.photo && lists.photo.secure_url} alt="Superior REDU" />
          </td>
          <td>
            <button className="btn-publish" onClick={e => this.handlePublish(e, `${lists._id}`)}>
              Publish
            </button>
            <button className="btn-reject" onClick={e => this.handleDelete(e, `${lists._id}`)}>
              Reject
            </button>
          </td>
        </tbody>
      );
    });
    return (
      <div className="admin-story-section">
        <div className="admin-story-split">
          <div className="admin-story-left">
            <h3 className="title">Operations</h3>
            <div className="admin--operation">
              <Link to="/admin/article" className="decor-none">
                <h3>Article Operation</h3>
              </Link>
            </div>
            <div className="admin--operation">
              <Link to="/admin/story" className="decor-none">
                <h3>Story Operation</h3>
              </Link>
            </div>
            <div className="admin--operation">
              <Link to="/admin/community" className="decor-none">
                <h3>Community Operation</h3>
              </Link>
            </div>
          </div>
          <div className="admin-story-right">
            <div className="admin--container">
              <div className="story--status">
                <h3>Story Status</h3>
                <div className="story--table">
                  <table>
                    <thead>
                      <th>User</th>
                      <th>Title</th>
                      <th>Body</th>
                      <th>Image</th>
                      <th>Status</th>
                    </thead>
                    {listPublish}
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.adminReducer.listStoryAdmin
  };
};

export default connect(
  mapStateToProps,
  { getStoryAdmin, publishStoryAdmin, deleteStoryAdmin }
)(withRouter(AdminStoryComponent));
