import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { postStoryAdmin } from "../../store/actions/admin/adminAction";
import "../../assets/scss/AdminStoryComponent.scss";

class AdminArticleComponent extends Component {
  constructor() {
    super();
    this.state = {
      kota: [
        "Ambon",
        "Balikpapan",
        "Bandar Lampung",
        "Bandung",
        "Batam",
        "Binjai",
        "Bogor",
        "Cirebon",
        "Denpasar",
        "Dumai",
        "Jambi",
        "Magelang",
        "Medan",
        "Padang",
        "Palembang",
        "Palu",
        "Pekanbaru",
        "Semarang",
        "Yogyakarta"
      ],
      title: "",
      body: "",
      category: "",
      location: "",
      media: null
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  mediaChange = e => {
    this.setState({
      media: e.target.files[0]
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    const imageFD = new FormData();
    imageFD.append(
      "file",
      this.state.media,
      this.state.media.name,
      this.state.media.type
    );
    const data = {
      title: this.state.title,
      body: this.state.body,
      category: this.state.category,
      location: this.state.location
    };
    const dataImage = {
      file: imageFD
    };
    await this.props.postStoryAdmin(data, dataImage);
    alert("Upload Article Success");
    // this.setState({
    //   title: "",
    //   body: "",
    //   media: null
    // });
    this.props.history.push("/article");
  };

  render() {
    const listKota = this.state.kota.map(index => (
      <option value={index} key={index}>
        {index}
      </option>
    ));
    return (
      <div className="admin-story-section">
        <div className="admin-story-split">
          <div className="admin-story-left">
            <h3 className="title">Operations</h3>
            <div className="admin--operation">
              <Link to="/admin/article" className="decor-none">
                <h3>Article Operation</h3>
              </Link>
            </div>
            <div className="admin--operation">
              <Link to="/admin/story" className="decor-none">
                <h3>Story Operation</h3>
              </Link>
            </div>
            <div className="admin--operation">
              <Link to="/admin/community" className="decor-none">
                <h3>Community Operation</h3>
              </Link>
            </div>
          </div>
          <div className="admin-story-right">
            <div className="admin--container">
              <div className="story--status">
                <h3>Post Article</h3>
                <div className="article--post">
                  <div className="box--post">
                    <h4>Title</h4>
                    <input
                      type="text"
                      name="title"
                      placeholder="Title"
                      value={this.state.title}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="box--post">
                    <h4>Body</h4>
                    <textarea
                      type="text"
                      name="body"
                      placeholder="Body"
                      value={this.state.body}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="box--post">
                    <h4>Image</h4>
                    <input
                      type="file"
                      name="image"
                      id=""
                      onChange={this.mediaChange}
                    />
                  </div>
                  <div className="box--post divide-box">
                    <div className="box-category">
                      <h4>Category</h4>
                      <select
                        name="category"
                        id=""
                        onChange={this.handleChange}
                      >
                        <option value="City">City</option>
                        <option value="Culinary">Culinary</option>
                        <option value="Culture">Culture</option>
                        <option value="Historical">Historical</option>
                        <option value="Natural">Natural</option>
                        <option value="Religious">Religious</option>
                        <option value="Tourism">Tourism</option>
                      </select>
                    </div>
                    <div className="box-location">
                      <h4>Location</h4>
                      <select
                        name="location"
                        id=""
                        onChange={this.handleChange}
                      >
                        {listKota}
                      </select>
                    </div>
                  </div>
                  <div className="box--post">
                    <button onClick={this.handleSubmit}>Post</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { postStoryAdmin }
)(withRouter(AdminArticleComponent));
