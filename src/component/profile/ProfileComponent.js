import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getProfile } from "../../store/actions/profileAction";
import { getMyStory } from "../../store/actions/story/listStoryAction";
import "../../assets/scss/ProfileComponent.scss";

class ProfileComponent extends Component {
  componentDidMount() {
    this.props.getProfile();
    this.props.getMyStory();
  }

  state = {
    loading: false
  };

  loading = isLoading => {
    if (isLoading) {
      return (
        <img
          src={require("../../assets/images/Spinner-1s-200px.gif")}
          style={{ textAlign: "center" }}
          alt="Loading"
        />
      );
    }
  };

  render() {
    console.log(this.props.data);
    console.log(this.props.myStory);
    const { _id, name, interest, photo, about, location } = this.props.data;
    const listInterest = interest && interest.map(lists => <p key={lists}>{lists}</p>);

    const profile = () => {
      return (
        <div className="profile--lists">
          <div className="profile-wrap-image">
            <img src={photo && photo.secure_url} alt="Superior REDU" />
          </div>
          <div className="profile-wrap">
            <h3>{name}</h3>
            {listInterest}
          </div>
          <div className="profile-wrap">
            <h4>About Me</h4>
            <p>{about ? about : `Im interest at ${interest}`}</p>
          </div>
          <div className="profile-wrap">
            <h4>Location</h4>
            <p>{location ? location : "Location Unknown"}, Indonesia</p>
          </div>
          <div className="profile-wrap">
            <Link to="/myprofile/edit" className="decor-none">
              <button>Edit Profile</button>
            </Link>
          </div>
        </div>
      );
    };
    return (
      <div className="profile--section" key={_id}>
        <div className="profile--container">
          <div className="profile--background">
            <img src={require("../../assets/images/2789561.jpg")} alt="Superior REDU" />
          </div>
          <div className="profile--wrapper">
            {profile()}
            <div className="profile-content-wrapper">
              <div className="profile--stories">
                <div className="profile-list-stories">
                  <h4>Stories you've been posted</h4>
                  <Link to="/myprofile/post" className="decor-none">
                    <div className="profile-post-story">
                      <img
                        src={require("../../assets/images/luigi-liccardo-y0bUB8jcUio-unsplash.jpg")}
                        alt="Superior REDU"
                      />
                      <h3>Post A Story</h3>
                    </div>
                  </Link>
                  <div className="profile-list-story"></div>
                </div>
                <div className="profile-list-communities">
                  <h4>Favorites Community</h4>
                  <div className="profile-list-community">
                    <div className="communities">
                      <img src={require("../../assets/images/nightowl.png")} alt="Superior REDU" />
                      <h5>Night Owl</h5>
                      <p>A place for night owl programmer.</p>
                    </div>
                    <div className="communities">
                      <img src={require("../../assets/images/sampan.jpg")} alt="Superior REDU" />
                      <h5>Basket Knight</h5>
                      <p>Knight of basketball</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.profileReducer.data,
    myStory: state.listStoryReducer.myStory
  };
};

export default connect(
  mapStateToProps,
  { getProfile, getMyStory }
)(ProfileComponent);
