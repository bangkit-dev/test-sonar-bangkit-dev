import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { getProfile } from "../../store/actions/profileAction";
import { editMyProfile } from "../../store/actions/profileAction";

class ProfileEditComponent extends Component {
  componentDidMount() {
    this.props.getProfile();
    this.props.editMyProfile();
  }

  constructor() {
    super();
    this.state = {
      kota: [
        "Ambon",
        "Balikpapan",
        "Bandar Lampung",
        "Bandung",
        "Batam",
        "Binjai",
        "Bogor",
        "Cirebon",
        "Denpasar",
        "Dumai",
        "Jambi",
        "Magelang",
        "Medan",
        "Padang",
        "Palembang",
        "Palu",
        "Pekanbaru",
        "Semarang",
        "Yogyakarta"
      ],
      name: "",
      about: "",
      interest: [],
      category: "",
      location: "",
      media: null
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  mediaChange = e => {
    this.setState({
      media: e.target.files[0]
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    const imageFD = new FormData();
    imageFD.append(
      "file",
      this.state.media,
      this.state.media.name,
      this.state.media.type
    );
    const data = {
      name: this.state.name,
      about: this.state.about,
      interest: this.state.interest,
      category: this.state.category,
      location: this.state.location
    };
    const dataImage = {
      file: imageFD
    };
    await this.props.editMyProfile(data, dataImage);
    alert("Update Profile Completed");
    // this.setState({
    //   title: "",
    //   body: "",
    //   media: null
    // });
    this.props.history.push("/profile");
  };

  render() {
    console.log(this.props.data);
    const { _id, name, interest, photo, about, location } = this.props.data;
    const listInterest =
      interest && interest.map(lists => <p key={lists}>{lists}</p>);
    const listKota = this.state.kota.map(index => (
      <option value={index} key={index}>
        {index}
      </option>
    ));
    return (
      <div className="profile--section" key={_id}>
        <div className="profile--container">
          <div className="profile--background">
            <img
              src={require("../../assets/images/2789561.jpg")}
              alt="Superior REDU"
            />
          </div>
          <div className="profile--wrapper">
            <div className="profile--lists">
              <div className="profile-wrap-image">
                <img src={photo && photo.secure_url} alt="Superior REDU" />
              </div>
              <div className="profile-wrap">
                <h3>{name}</h3>
                {listInterest}
              </div>
              <div className="profile-wrap">
                <h4>About Me</h4>
                <p>{about ? about : `Im interest at ${interest}`}</p>
              </div>
              <div className="profile-wrap">
                <h4>Location</h4>
                <p>{location ? location : "Location Unknown"}, Indonesia</p>
              </div>
              <div className="profile-wrap">
                <Link to="/myprofile/edit" className="decor-none">
                  <button>Edit Profile</button>
                </Link>
              </div>
            </div>
            <div className="profile-content-wrapper">
              <div className="profile--stories">
                <div className="profile-list-stories">
                  <h4>Edit Profile</h4>
                  <form onSubmit={this.handleSubmit}>
                    <div className="profile-post-stories">
                      <div className="box--story">
                        <h4>Name</h4>
                        <input
                          type="text"
                          name="name"
                          value={this.state.title}
                          onChange={this.handleChange}
                          placeholder={name}
                          required
                        />
                      </div>
                      <div className="box--story">
                        <h4>Description</h4>
                        <textarea
                          name="about"
                          value={this.state.body}
                          onChange={this.handleChange}
                          id=""
                          cols="30"
                          rows="5"
                          placeholder="Describe yourself here..."
                          required
                        ></textarea>
                      </div>
                      <div className="box--story">
                        <h4>Change Photo</h4>
                        <input
                          type="file"
                          name="image"
                          id=""
                          onChange={this.mediaChange}
                          required
                        />
                      </div>
                      <div className="box-story-wrap divide-box">
                        <div className="box--story mb-20">
                          <h4>Gender</h4>
                          <select
                            name="gender"
                            id=""
                            onChange={this.handleChange}
                            required
                          >
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                          </select>
                        </div>
                        <div className="box--story mb-20">
                          <h4>Interest</h4>
                          <select
                            name="interest"
                            id=""
                            required
                            onChange={this.handleChange}
                          >
                            <option value="City Tourism">City Tourism</option>
                            <option value="Culinary">Culinary</option>
                            <option value="Culture">Culture</option>
                            <option value="Historical">Historical</option>
                            <option value="Nature">Nature</option>
                            <option value="Religious">Religious</option>
                            <option value="Tourism">Tourism</option>
                          </select>
                        </div>
                        <div className="box--story">
                          <h4>Location</h4>
                          <select
                            name="location"
                            id=""
                            required
                            onChange={this.handleChange}
                          >
                            {listKota}
                          </select>
                        </div>
                      </div>
                      <div className="box--story">
                        <button type="submit">Update My Profile</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.profileReducer.data
  };
};

export default connect(
  mapStateToProps,
  { getProfile, editMyProfile }
)(withRouter(ProfileEditComponent));
