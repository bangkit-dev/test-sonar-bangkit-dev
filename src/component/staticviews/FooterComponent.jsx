import React, { Component } from "react";
import "../../assets/scss/FooterComponent.scss";

class FooterComponent extends Component {
  render() {
    return (
      <div className="footer--section">
        <div className="footer--container">
          <div className="footer--items">
            <div className="footer--item">
              <img src={require("../../assets/images/Group-1.png")} alt="Superior REDU" />
            </div>
            <div className="footer--item">
              <p>About Us</p>
              <p>Blog</p>
            </div>
            <div className="footer--item">
              <p>Article</p>
              <p>Story</p>
              <p>Community</p>
            </div>
            <div className="footer--item">
              <p>Contact Us</p>
              <span>contact@redu.id</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FooterComponent;
