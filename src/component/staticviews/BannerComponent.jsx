import React, { Component } from "react";
import "../../assets/scss/BannerComponent.scss";

class BannerComponent extends Component {
  render() {
    return (
      <div className="banner-section">
        <div className="banner-container">
          <h1>Have a Great Day</h1>
        </div>
      </div>
    );
  }
}

export default BannerComponent;
