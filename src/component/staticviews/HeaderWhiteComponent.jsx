import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Button, Box } from "@material-ui/core";
import "../../assets/scss/HeaderWhiteComponent.scss";

class HeaderWhiteComponent extends Component {
  constructor() {
    super();
    this.state = {
      category: ""
    };
  }

  keyPressed = e => {
    if (e.key === "Enter") {
      this.handleSubmit();
    }
  };

  handleSubmit = e => {
    this.props.history.push(`/content/category/${this.state.category}`);
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleLogout = e => {
    sessionStorage.removeItem("token");
    this.props.history.push("/");
    window.location.reload(false);
  };

  headerNavbar = () => {
    if (this.props.isAuthenticated || sessionStorage.token != null) {
      return (
        <Box>
          <Link className="decor-none" onClick={this.handleLogout} to="">
            <Button style={{ margin: "5px" }} variant="contained" color="primary">
              Logout
            </Button>
          </Link>
          <Link className="decor-none" to="/profile">
            <Button variant="contained" color="primary">
              Profile
            </Button>
          </Link>
        </Box>
      );
    } else {
      return (
        <Box>
          <Link className="decor-none" to="/login">
            <Button style={{ margin: "5px" }} variant="contained" color="primary">
              Login
            </Button>
          </Link>
          <Link className="decor-none" to="/register">
            <Button variant="contained" color="primary">
              Register
            </Button>
          </Link>
        </Box>
      );
    }
  };

  render() {
    // return (
    //   <AppBar position="static">
    //     <Toolbar>
    //       <Link className="decor-none" to="/">
    //         <img src={require("../../assets/images/Group-1.png")} alt="" />
    //       </Link>
    //       <Box p={1} flexGrow={1} display="flex" justifyContent="flex-end">
    //         <Box p={1}>
    //           <Paper>
    //             <InputBase
    //               style={{ padding: "5px" }}
    //               placeholder="Search...."
    //             />
    //           </Paper>
    //         </Box>
    //         <Box p={1}>{this.headerNavbar()}</Box>
    //       </Box>
    //     </Toolbar>
    //   </AppBar>
    // );
    return (
      <div className="header-white">
        <div className="header-container">
          <div className="header-logo">
            <Link className="decor-none" to="/">
              <img src={require("../../assets/images/Group-1.png")} alt="" />
            </Link>
          </div>
          {/* <div className="header-search">
            <input
              type="search"
              name="category"
              placeholder="Search"
              onChange={this.handleChange}
              onKeyPress={this.keyPressed}
              value={this.state.category}
            />
          </div> */}
          {this.headerNavbar()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated
  };
};

export default connect(mapStateToProps)(withRouter(HeaderWhiteComponent));
