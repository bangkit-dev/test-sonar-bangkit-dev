import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getStoryById } from "../../store/actions/story/listStoryAction";
import { getProfile } from "../../store/actions/profileAction";
import "../../assets/scss/DetailArticleComponent.scss";

class DetailStoryComponent extends Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.getStoryById(id);
    this.props.getProfile();
  }

  commentProfile = () => {
    if (this.props.isAuthenticated || sessionStorage.token != null) {
      return (
        <img
          src={this.props.profile.photo && this.props.profile.photo.secure_url}
          alt={this.props.profile.name}
        />
      );
    } else {
      return (
        <img
          src={require("../../assets/images/user-png-icon-male-user-icon-512.png")}
          alt="Login First"
        />
      );
    }
  };

  constructor() {
    super();
    this.state = {
      text: ""
    };
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    const postComment = {
      text: this.state.text
    };

    this.props.postComment(postComment, this.props.data._id);
    this.setState({
      text: ""
    });
  };

  render() {
    const { title, photo, body, category, _user, location } = this.props.data;
    return (
      <div className="detail-article-section">
        <div className="detail--container">
          <div className="detail--items">
            <div className="detail-item-img">
              <img src={photo && photo.secure_url} alt={title} />
            </div>
            <div className="detail-item-article">
              <div className="profile">
                <img src={_user && _user.photo.secure_url} alt="John Doe" />
                <h4>{_user && _user.name}</h4>
              </div>
              <div className="article">
                <h3>{title}</h3>
                <p className="category">
                  Category: <span>{category}.</span> On {location}
                </p>
                <div className="box">
                  <p className="text">{body}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="detail-another-lists">
            <div className="detail-list-items">
              <h3>Another stories that seems like this</h3>
              <div className="detail-list-all">
                <div className="detail-list-img">
                  <img
                    src={require("../../assets/images/wisata-300x200.jpg")}
                    alt="Superior REDU"
                  />
                </div>
                <div className="detail-list-item">
                  <h4>Farmhouse Lembang Bandung</h4>
                  <p className="name">Eloona</p>
                  <p className="date">Posted on 07 Nov 2019</p>
                </div>
              </div>
              <div className="detail-list-all">
                <div className="detail-list-img">
                  <img
                    src={require("../../assets/images/wisata1-300x180.jpg")}
                    alt="Superior REDU"
                  />
                </div>
                <div className="detail-list-item">
                  <h4>Taman Bunga Begonia Lembang</h4>
                  <p className="name">Eloona</p>
                  <p className="date">Posted on 07 Nov 2019</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.listStoryReducer.detailStory,
    comment: state.listStoryReducer.storyComment,
    profile: state.profileReducer.data,
    isAuthenticated: state.authReducer.isAuthenticated
  };
};

export default connect(
  mapStateToProps,
  { getStoryById, getProfile }
)(withRouter(DetailStoryComponent));
