import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Link } from "react-router-dom";
import { getListStory } from "../../store/actions/story/listStoryAction";
import "../../assets/scss/StoryComponent.scss";

class StoryComponent extends Component {
  componentDidMount() {
    this.props.getListStory();
  }

  loading = () => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
          margin: "auto"
        }}
      >
        <img
          src={require("../../assets/images/Spinner-1s-200px.gif")}
          style={{ textAlign: "center" }}
          alt="Loading"
        />
      </div>
    );
  };

  render() {
    const listStory = this.props.listStory.map(lists => {
      return (
        <Link className="decor-none" key={lists._id} to={`/story/${lists._id}`}>
          <div className="story-item">
            <img
              src={lists.photo && lists.photo.secure_url}
              alt={lists.title}
            />
            <div className="story-item-list">
              <div className="story-item-img">
                <img
                  src={lists._user && lists._user.photo.secure_url}
                  alt={lists._user && lists._user.name}
                />
              </div>
              <div className="story-item-detail">
                <h4>{lists.title}</h4>
                <p className="capital">
                  Category: {lists.category}. Location: {lists.location}.
                </p>
                <p>Posted On {moment(lists.date).format("LL")}.</p>
              </div>
            </div>
          </div>
        </Link>
      );
    });
    console.log(this.props.listStory);
    return (
      <div className="story--section">
        <div className="story--container">
          <div className="story--header">
            <h3>Explore world from here</h3>
            <div className="story-header-category">
              <h4>Sort by Category: </h4>
              <select name="category" id="">
                <option value="All">All</option>
                <option value="City">City</option>
                <option value="Culinary">Culinary</option>
                <option value="Culture">Culture</option>
                <option value="Historical">Historical</option>
                <option value="Natural">Natural</option>
                <option value="Religious">Religious</option>
                <option value="Tourism">Tourism</option>
              </select>
            </div>
          </div>
          <div className="story--body">
            {listStory.length >= 1 ? (
              listStory
            ) : this.props.loading ? (
              this.loading()
            ) : (
              <p className="Grey">Story is empty...</p>
            )}
          </div>
          <br />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    listStory: state.listStoryReducer.listStory,
    loading: state.listStoryReducer.loading
  };
};

export default connect(
  mapStateToProps,
  { getListStory }
)(StoryComponent);
