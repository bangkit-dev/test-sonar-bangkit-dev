import React, { Component } from "react";
import "../assets/scss/LandingComponent.scss";

class LandingComponent extends Component {
  render() {
    return (
      <div className="jumbotron--section">
        <div className="jumbotron--container">
          <div className="jumbotron--title">
            <h1>Hi, we are Redu</h1>
            <p>
              Recreation Education (REDU) is a platform for people to share their travel story. A
              guest book, a place where people record their holiday experience. Don't let the
              experience just for yourself, let the others know. Share it, with REDU.
            </p>
          </div>
          <div className="jumbotron--img">
            <img src={require("../assets/images/undraw_airport_2581.svg")} alt="Superior REDU" />
          </div>
        </div>
      </div>
    );
  }
}

export default LandingComponent;
