import React, { Component } from "react";
import { connect } from "react-redux";
import { getListCommunity } from "../../store/actions/community/communityAction";
import "../../assets/scss/CommunityComponent.scss";

class CommunityComponent extends Component {
  componentDidMount() {
    this.props.getListCommunity();
  }

  render() {
    console.log(this.props.data);
    const listCommunity = this.props.data.map(lists => {
      return (
        <div className="community-list-wrap" key={lists._id}>
          <div className="community-list-img">
            <img src={lists.photo && lists.photo.secure_url} alt={lists.name} />
          </div>
          <div className="community-list-detail">
            <h3>{lists.name}</h3>
            <p>{lists.description}</p>
            <a href={`${lists.url}`} target="_blank" className="decor-none">
              <button>Go To Link</button>
            </a>
          </div>
        </div>
      );
    });
    return (
      <div className="community--section">
        <div className="community--jumbotron">
          <div className="community-jumbotron-container">
            <div className="community--description">
              <h3>Find other people that have same interest with you</h3>
              <p>
                There are many community in every destinations. And it will be good for you to ask
                every details about your destination from the community. That's why we collect every
                community information and share it with you. Don't hestitate to contact them, and
                start your journey.
              </p>
            </div>
            <div className="community--img">
              <img
                src={require("../../assets/images/undraw_community_8nwl.svg")}
                alt="Superior REDU"
              />
            </div>
          </div>
        </div>
        <div className="community--lists">
          <div className="community--container">{listCommunity}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.communityReducer.listCommunity
  };
};

export default connect(
  mapStateToProps,
  { getListCommunity }
)(CommunityComponent);
